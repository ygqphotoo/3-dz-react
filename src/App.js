import React from "react";
import LoadProducts from "./Products_component/LoadProducts";
import "./Products_component/Products.css"

import { BrowserRouter as Router, Link } from "react-router-dom"

function App (){
    return(
      <Router>
        <div className="App">
        <li className="header_li"><Link className="header_link" to="/">Main</Link></li>
        <li className="header_li"><Link className="header_link" to="/cart">Cart</Link></li>
        <li className="header_li"><Link className="header_link" to="/favorite">Favorite</Link></li>
        <LoadProducts  />
        </div>
      </Router>
    )
  }

export default App;
