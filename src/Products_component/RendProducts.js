import React from "react";
import Button from "../Button_component/Button";
import {AiOutlineStar} from "react-icons/ai";

function RendProducts (props) {
    return(
        <div key = {props.index} className="product_position">  
            <div className="products_set">
            <p className="product_name">{props.title}</p>
            <p className="product_price">{props.price}</p>
            <img className="product_img" src={props.url} alt="product_image"/>
            <p className="product_article">Article: {props.article}</p>
            <p className="product_color">Color: {props.color}</p>
            <Button className="propsBtn"
                onClick = {() => {props.handleModalOpen(props.id)}}
                text = {"Add to cart"}>
            </Button>
            <AiOutlineStar
                onClick = {() => props.isInFavorites ? props.removeProductFromFavorite(props.id) : props.addProductToFavorite(props.id)}
                className = {props.isInFavorites ? "favorite": " "}/>
            </div>
        </div>
    )
}

export default RendProducts;