import React, { useState, useEffect } from "react";
import "./Products.css"
import Modal from "../Modal_component/Modal";
import { Switch, Route } from "react-router-dom";
import Cart from "../Router/Cart";
import Favorite from "../Router/Favorite";
import PropsProducts from "./PropsProducts";

function LoadProducts (){
    const [products, setProducts] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [currentProductId, setCurrentProductId] = useState(null);
    const [currentProduct, setCurrentProduct] = useState(false);

    async function loadProducts(){
        const response = await fetch("./Product.json")
        const data = await response.json()
        const localCart = JSON.parse(localStorage.getItem("cart"))
        const localFavorite = JSON.parse(localStorage.getItem("favorite"))
        const updatedProducts = checkProducts(data.products , localCart , localFavorite)
        const updateStorage = updatedStorage (updatedProducts);
        setProducts(updatedProducts)
    }

    function updatedStorage (updatedProducts) {
        updatedProducts.forEach(element => {
            if(element.isInCart === "true"){
                localStorage.setItem("cart", JSON.stringify(updatedProducts));
            }
        });
    }

    useEffect (() => {
        if (!localStorage.getItem("cart") || !localStorage.getItem("favorite"))  {
        localStorage.setItem("cart", JSON.stringify([]))
        localStorage.setItem("favorite", JSON.stringify([]))
        }
        loadProducts()
    },[])

    function checkProducts(products, cart, favorite){
        const updatedProducts = products.map(product =>{
            const productInCart = cart.some(({id}) => (id === product.id))
            const productInFavorite = favorite.some(({id}) => (id === product.id))
            return {...product, 
                isInCart: productInCart, 
                isInFavorites: productInFavorite}
        })
        return updatedProducts
    }

    function handleModalOpen(id){
        if(id){
            setIsOpen(!isOpen);
            setCurrentProductId(id)
        }else{
            setIsOpen(!isOpen)
            setCurrentProductId(null);
            
        }
    }
    function addProductToCart(id){
        const cart = JSON.parse(localStorage.getItem("cart"))
        const product = products.find(item => (item.id === id))
        cart.push(product)
        localStorage.setItem("cart", JSON.stringify(cart))
        setIsOpen(!isOpen);
        setCurrentProductId(id)
        

        const cartList = products.map((item) =>{
            if(item.id === id){
                item.isInCart = true
            }
            return item
        })
        setProducts(cartList)
    }
    function addProductToFavorite(id){    
        const favorite = JSON.parse(localStorage.getItem("favorite"))
        const product = products.find(item => (item.id === id))          
        favorite.push(product)
        localStorage.setItem("favorite", JSON.stringify(favorite))

        const favoriteList = products.map((item) => {   
            console.log(item.isInFavorites)
            if(item.id === id && item.isInFavorites === false){
                item.isInFavorites = true
            }
            return item
        })
        setCurrentProduct(!currentProduct);
        setCurrentProductId(id);
        setProducts(favoriteList)
    }

    function removeProductFromFavorite(id){
        const favorite = JSON.parse(localStorage.getItem("favorite"))
        favorite.map((item, index) => {
            if(id === item.id){
                favorite.splice(index, 1)
            }
            return favorite
        })
        localStorage.setItem("favorite", JSON.stringify(favorite))
        const product = products.map((item) => {
            if(id === item.id){
                item.isInFavorites = false
            }
            return item
        })
        setCurrentProduct (!currentProduct);
        setProducts(product)
    }
    function removeFromCart(id) {
        const productToDelete = JSON.parse(localStorage.getItem('cart'))
        productToDelete.map((product, index) => {
            if (id === product.id) {
                productToDelete.splice(index, 1)
            }
            return productToDelete
        })
        localStorage.setItem('cart', JSON.stringify(productToDelete))
        setIsOpen(!isOpen);
        const cartList = products.map(product => {
            if (id === product.id) {
                product.isInCart = false
            }
            return product
    })
}

        return(
            <div className="products"> 
                <Switch>
                    <Route exact path="/">
                        <PropsProducts product = {products}
                        handleModalOpen = {(id) => handleModalOpen(id)}
                        addProductToFavorite = {(id) => {addProductToFavorite(id)}}
                        removeProductFromFavorite = {(id) => {removeProductFromFavorite(id)}}
                    />
                    <Modal             
                        id = {currentProductId}
                        isOpen = {isOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {(id) =>{handleModalOpen(id)}}
                        onSubmit = {(id) =>{addProductToCart(id)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                    </Route>
                <Route path="/cart">
                    <Cart 
                        products = {products.filter(product => {return product.isInCart})}
                        handleModalOpen = {(id) => {handleModalOpen(id)}}
                        addProductToFavorite = {(id) => {addProductToFavorite(id)}}
                        removeProductFromFavorite = {(id) => {removeProductFromFavorite(id)}}
                        buttonTitle = 'remove'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isOpen}
                        header = {"Хотите удалить с корзины?"}
                        onCancel = {(id) =>{handleModalOpen(id)}}
                        onSubmit = {(id) =>{removeFromCart(id)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                </Route>
                <Route path="/favorite">
                    <Favorite 
                        products = {products.filter(product => {return product.isInFavorites})}
                        handleModalOpen = {(id) => {handleModalOpen(id)}}
                        addProductToFavorite = {(id) => {addProductToFavorite(id)}}
                        removeProductFromFavorite = {(id) => {removeProductFromFavorite(id)}}
                        buttonTitle = 'add'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {(id) =>{handleModalOpen(id)}}
                        onSubmit = {(id) =>{addProductToCart(id)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"}                        
                    />
                </Route>
                </Switch>
            </div>
            
        )
}           

export default LoadProducts;