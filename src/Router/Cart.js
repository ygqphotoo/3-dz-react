import React from "react";
import "../Products_component/Products.css";
import {AiOutlineStar} from "react-icons/ai";



function Cart (props){    
    return(
        <>
            {props.products &&
                props.products.map((element, index)  => {
                return(
                 <div key={index.toString()} className="product_position">
                 <button className="btn_delete"
                  onClick= {(evt) => {
                  evt.preventDefault();
                  props.handleModalOpen(element.id);                         
                  }}
                  >X</button> 
             <div className="products_set">
                <p className="product_name">{element.name}</p>
                <p className="product_price">{element.price}</p>
                <img className="product_img" src={element.url} alt="product_image"/>
                <p className="product_article">Article: {element.article}</p>
                <p className="product_color">Color: {element.color}</p>
             </div>  
             <AiOutlineStar
                onClick = {() => props.isInFavorites ? props.removeProductFromFavorite(element.id) : props.addProductToFavorite(element.id)}
                className = {element.isInFavorites ? "favorite": ""}/>                                  
            </div> 
                        )
             })} 
        </>
    )
}

export default Cart;