import React from "react";
import "../Products_component/Products.css";
import {AiOutlineStar} from "react-icons/ai";
import Button from "../Button_component/Button";

function Favorite (props){
        return(
            <>
                    {props.products && props.products.map((el, key) => {
                        return(
                            <div key={key.toString()} className="product_position">
                                <div className="products_set">
                                    <p className="product_name">{el.name}</p>
                                    <p className="product_price">{el.price}</p>
                                    <img className="product_img" src={el.url} alt="product_image"/>
                                    <p className="product_article">Article: {el.article}</p>
                                    <p className="product_color">Color: {el.color}</p>
                                    <Button className="propsBtn"
                                    onClick = {() => {props.handleModalOpen(el.id)}}
                                    text = {"Add to cart"}>
                                    </Button>   
                                    <AiOutlineStar
                                     onClick = {() => props.removeProductFromFavorite(el.id)}
                                     className = {el.isInFavorites ? "favorite": ""}/>
                                </div>
                            </div>
                        )
                    })
                }
            </>
        )
    }

export default Favorite;